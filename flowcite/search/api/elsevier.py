from .common_api import CommonAPI


class Elsevier(CommonAPI):

    def __init__(self):
        super().__init__(api_endpoint="https://api.elsevier.com/content/search/scidir")
        return

    def __str__(self):
        return "Elsevier"
