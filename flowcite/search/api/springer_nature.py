from .common_api import CommonAPI


class SpringerNature(CommonAPI):

    def __init__(self):
        super().__init__(
            api_endpoint="http://api.springernature.com/metadata/json?q=",
            api_key="c1735516a63ea19947c4a7eef144046e"
        )
        return

    def __str__(self):
        return "SpringerNature"

    def set_result_number(self, number=10):
        return "p={}&".format(number)

    def get_search_result(self, search_data_set):
        query = ""
        for data in search_data_set:
            query += self.add_attribute(data["attribute"], data["value"], ":")

        query += self.set_result_number(2)
        query += self.add_apiKey(prefix="api_key")
        url = self.api_endpoint + query
        return url
