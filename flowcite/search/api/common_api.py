import requests


class CommonAPI:

    def __init__(self, api_endpoint="", api_key=""):
        self.api_endpoint = api_endpoint
        self.api_key = api_key
        return

    def __str__(self):
        return "CommonAPI"

    def add_attribute(self, attribute,  value, separator="="):
        value = "%20".join(value.split(" "))
        return "{}{}{}&".format(attribute, separator, value)

    def add_apiKey(self, prefix):
        return "{}={}&".format(prefix, self.api_key)

    def get_api_data(self, url):
        return requests.get(url)
