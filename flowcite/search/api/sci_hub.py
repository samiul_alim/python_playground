import requests
from html.parser import HTMLParser


class Scihub(HTMLParser):
    sci_hub_url = "http://www.sci-hub.tw/"

    def __init__(self, doi):
        super().__init__()
        self.doi = doi

    def __str__(self):
        return "Scihub"

    def handle_starttag(self, tag, attrs):
        if tag == "iframe":
            for attr in attrs:
                if attr[0] == "src":
                    return attr[1]

    def get_pdf_link(self, doi):
        html = requests.get(self.sci_hub_url + doi)
        pdf_link = self.feed(html.text)
        print("PDF LINK ---> ".format(pdf_link))
        return pdf_link
