from django.shortcuts import render, HttpResponse
from .search.search import Search
from django.utils.safestring import mark_safe
import json

# Create your views here.

class SearchView:

    def index(request):
        return render(request, "index.html", {})

    def search(request):
        data_set = json.loads(request.POST.get('data'))
        search = Search()
        result = search.do_search(data_set)
        return HttpResponse('hi', content_type="application/json")

    def chat_view(request):
        return render(request, "chat.html", {})

    def room(request, room_name):
        return render(request, 'room.html', {
            'room_name_json': mark_safe(json.dumps(room_name))
        })
