from django.apps import AppConfig


class FlowcitConfig(AppConfig):
    name = 'flowcite'
