from django.urls import path
from .views import SearchView
from django.views.decorators.csrf import csrf_exempt


urlpatterns = [
    # path('', SearchView.index),
    path('', SearchView.chat_view),
    path('chat/<room_name>', SearchView.room),
    path('search', csrf_exempt(SearchView.search)),
]